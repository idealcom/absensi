
<!-- <link rel="stylesheet" href="<?php echo base_url('assets') ?>/global/css/style.css"> -->
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/global/css/prism.css">
<link rel="stylesheet" href="<?php echo base_url('assets') ?>/global/css/chosen.css">

<script type="text/javaScript"> 
	$(document).ready(function(){   
	});
</script>
<style type="text/css" media="all">
	/* fix rtl for demo */
	.chosen-rtl .chosen-drop { left: -9000px; }
</style>
<div class="portlet-body form">
	<i class="fa fa-cogs font-green-sharp"></i>
	<span class="caption-subject font-green-sharp bold uppercase">Data Absensi</span>
	<br><br>
	<br>
	<?php if ($this->uri->segment(2)=="inputdata") {
		echo "inputdata";
	}
	else
		{ ?>
	<form action="<?php echo site_url('absensi/searchdata') ?>" id="formsearch" method="post">
		<div class="form-group">
			<label class="col-md-3 control-label">Tahun Ajaran</label>
			<div class="col-md-9" style="width:40%;">
				<select class="form-control" id="lbl_ajaran" name="lbl_ajaran">
					<?php foreach ($th_ajaran as $row): ?>
						<option value="<?=$row->kd_ajaran ?>"><?php echo $row->lbl_ajaran; ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<br>
		<br>
		<br>
		<div class="form-group">
			<label class="col-md-3 control-label">Kelas</label>
			<div class="col-md-9">
				<select data-placeholder="Pilih Kelas" class="chosen-select form-controle" style="width:350px;" tabindex="2" id="kelas" name="kelas">
					<option value=""></option>
					<?php foreach ($kelas as $row): ?>
						<option value="<?php echo $row->kelas; ?>">Kelas <?php echo $row->kelas; ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
</div>
</div>
<br>
<br>
<div class="tools">
	<a href="javascript:;" class="collapse">
	</a>
	<a href="#portlet-config" data-toggle="modal" class="config">
	</a>
	<a href="javascript:;" class="reload">
	</a>
	<a href="javascript:;" class="remove">
	</a>
</div>
</div>
<div class="col-md-6">
	<div class="btn-group">
		<button id="btncari" class="btn green">
			Cari <i class="fa fa-search"></i>
		</button>
	</div>
</div>
</form>
<div class="portlet-body">
	<div class="table-toolbar">
		<div class="row">
			<div class="col-md-6">
				<div class="btn-group pull-right">
					<button class="btn dropdown-toggle" data-toggle="dropdown">Export Data <i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu pull-right">
					<li>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'json',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/json.png' width='24px'>JSON</a>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='<?php echo base_url('assets') ?>/imgs/json.png' width='24px'> JSON (ignoreColumn)</a>
							
						</li>
						<hr></hr>
						<li>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'xml',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/xml.png' width='24px'>XML</a>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'sql'});"><img src='<?php echo base_url('assets') ?>/imgs/sql.png' width='24px'>SQL</a>
							
						</li>
						<hr></hr>
						<li>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'csv',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/csv.png' width='24px'>CSV</a>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'txt',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/txt.png' width='24px'>TXT</a>
							
						</li>
						<hr></hr>
						<li>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'excel',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/excel.png' width='24px'>EXCEL</a>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'word',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/word.png' width='24px'>WORD</a>
							
						</li>
						<hr></hr>
						<li>
							<a class="#" onClick ="$('#exprotdata').tableExport({type:'pdf',escape:'false'});"><img src='<?php echo base_url('assets') ?>/imgs/pdf.png' width='24px'>PDF</a>
						</li>
						
						</ul>
					</div>
				</div>
			</div>
		</div>

		<form id="formsubmit" action="<?php echo site_url('absensi/formabsensi') ?>" method="post">
			<table class="table table-striped table-hover table-bordered" id="exprotdata">
				<thead>
					<tr>
						<th>
							<div align="center"></div>No
						</th>
						<th>
							<div align="center">Kode MK</div>
						</th>
						<th>
							<div align="center">Kelas</div>
						</th>
						<th>
							<div align="center">Nama MK</div>
						</th>
						<th>
							<div align="center">Dosen</div>
						</th>
						<th>
							<div align="center">Absen Data</div>
						</th>
						
					</tr>
				</thead>
				<tbody id="blo">
				</tbody>
			</table>
		</form>
	</div>
</div>
<?php } ?>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/plugins/jquery-1.7.js"></script>
<script src="<?php echo base_url('assets') ?>/global/scripts/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/global/scripts/prism.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/global/scripts/conf.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/admin/pages/scripts/table-editable.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url('assets') ?>/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets') ?>/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
		var btn = $("btncari").val();
		$("#btncari").click(function() {
			var action = $("#formsearch").attr('action');
			var form = {
				lbl_ajaran : $("#lbl_ajaran").val(),
				kelas : $("#kelas").val()
			};
			$.ajax({
				type : "POST",
				url  : action,
				data : form,
				dataType :'html',
				beforeSend : function() {
					$("#blo").html("<img src='<?php echo base_url('assets/global/img/loading-spinner-grey.gif') ?>'>"+"Sedang Mencari data...");
				},
				success : function(data) {
					$("#blo").html(data);
				}
			});
		});
		$("#btnInput").click(function() {
			console.log($(this));
		});
	});
</script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/tableExport.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/jquery.base64.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/sprintf.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/jspdf.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/global/scripts/base64.js"></script>


