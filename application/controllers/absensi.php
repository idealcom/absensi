<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Absensi extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("chz_helper");
	}
	public function index()
	{
		$post = $this->input->post();
		$lbl_ajaran=$this->input->post('tahun_ajaran');
		$kelas=$post['kelas'];
		$data['id'] = $this->model->search($lbl_ajaran,$kelas);
		$data['th_ajaran'] = libselect("table_thajaran");
		$data['kelas'] = $this->model->kelas();
		$data['fabsensi'] = $this->model->data_absensi();
		$this->load->view("absensi_v",$data);	
	}
	public function searchdata()
	{
		$post = $this->input->post();
		$lbl_ajaran=$this->input->post('tahun_ajaran');
		$kelas=$post['kelas'];
		$data['search'] = $this->model->search();
		if ($data['search']->num_rows()>0) {
			$no = 0;
			$array = array();
			foreach ($data['search']->result() as $row) {
				$no++;
				$array = array();
		// 	foreach ($data['search']->result() as $row) {
		// 	array[] = array("kode_mk"=>$row["kode_mk"],
		// 					"kelas"=>$row["kelas"],
		// 					"semester"=>$row["semester"]);
		// }
		// echo json_encode($array);
				echo "
				<tr>
					<td>".$no."</td>

					<td>
						<input type='hidden' name='fkode_mk' value='".$row->kode_mk."'>
						".$row->kode_mk."
					</td>
					<td>
						<input type='hidden' name='fkelas' value='".$row->kelas."'>
						".$row->kelas."
					</td>
					<td>
					<input type='hidden' name='fdsmt' value='".$row->semester."'>
					<input type='hidden' name='fmatkul' value='".$row->nama_mk."'>	
					".$row->nama_mk."
					</td>
					<td>
					<input type='hidden' name='fnama_dosen' value='".$row->nama_dosen."'>
					".$row->nama_dosen."
					</td>
				<td>
				<button class='icon-btn' id='btnInput'>
				<i class='fa fa-bar-chart-o'></i>
					<div>Absen Data</div>
				</button>
			</td>
		</tr>";
	}
}
else
{
	echo "Data tidak ditemukan";
}

}
public function formabsensi()
{
	$data['search'] = $this->model->search();
	$data['fabsensi'] = $this->model->data_absensi();
	$this->load->view("index",$data);
}
public function page()
{
	$data['fabsensi'] = $this->model->data_absensi();
	$this->load->view("index",$data);
}
}
// <td >
// 				<a href='".site_url("absensi/page/edit/".$row->kode_mk)."' class='edit'> Edit</a>
// 			</td>
// 			<td >
// 				Delete
// 			</td>